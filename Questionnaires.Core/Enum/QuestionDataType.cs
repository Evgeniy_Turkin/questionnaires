﻿namespace Questionnaires.Core.Enum
{
    /// <summary>
    /// Тип данных вопроса
    /// </summary>
    public enum QuestionDataType : byte
    {
        text = 0,

        number = 1,

        date = 2,

        select = 3,

        checkbox = 4
    }
}
