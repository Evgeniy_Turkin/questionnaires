﻿using Ninject;
using Questionnaires.Entities;
using Questionnaires.Infrastructure;
using Questionnaires.Infrastructure.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaires.BusinessLayer
{
    public class QuestionManager : IQuestionManager
    {
        private readonly IKernel _container;

        public QuestionManager(IKernel container)
        {
            _container = container;
        }

        public List<Question> ListQuestion()
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var repository = uow.GetRepository<Question>();
                var questions = repository.GetAll().ToList();

                return questions;
            }
        }

        public Question CreateQuestion(Question question)
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var repository = uow.GetRepository<Question>();
                repository.Add(question);
                repository.SaveChanges();
                return question;
            }
        }

        public Question EditQuestion(Question question)
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var repository = uow.GetRepository<Question>();
                var dbquestion = repository.SingleOrDefault<Question>(c => c.QuestionId == question.QuestionId);

                dbquestion.Text = question.Text;
                dbquestion.Type = question.Type;

                repository.Update(dbquestion);
                repository.SaveChanges();
                return question;
            }
        }

        public Question GetQuestion(int id)
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var repository = uow.GetRepository<Question>();
                var dbproject = repository.SingleOrDefault<Question>(c => c.QuestionId == id);
                return dbproject;
            }
        }

        //public void RemoveQuestion(Question question)
        //{
        //    throw new NotImplementedException();
        //}

        public void Dispose()
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var repository = uow.GetRepository<Question>();
                repository.Dispose();
            }
        }

        public void EditQuestion(List<Question> questions)
        {
            throw new NotImplementedException();
        }
    }
}
