﻿using Ninject;
using Questionnaires.Entities;
using Questionnaires.Infrastructure;
using Questionnaires.Infrastructure.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaires.BusinessLayer
{
    public class AnswerManager : IAnswerManager
    {
        private readonly IKernel _container;

        public AnswerManager(IKernel container)
        {
            _container = container;
        }

        public List<Answer> ListAnswers()
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var test = new List<Answer>();
                return test;
            }
        }

        public string Save(List<Question> questions)
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var repository = uow.GetRepository<Answer>();
                var answers = new List<Answer>();
                foreach (var item in questions)
                {
                    var answer = new Answer();
                    answer.QuestionId = item.QuestionId;
                    answer.Text = item.Answer;
                    answers.Add(answer);
                }
                repository.AddRange(answers);
                repository.SaveChanges();
                return "Ответы сохранены";
            }
        }

        public void Dispose()
        {
            using (var uow = _container.Get<IUnitOfWork>())
            {
                var repository = uow.GetRepository<Question>();
                repository.Dispose();
            }
        }

    }
}
