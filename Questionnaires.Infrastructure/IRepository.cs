﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Questionnaires.Infrastructure
{
    /// <summary>
    /// Repository по работе с сущностями базы данных
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// Добавляет новую запись
        /// </summary>
        /// <param name="obj"></param>
        void Add(TEntity obj);

        /// <summary>
        /// Добавляет коллекцию
        /// </summary>
        /// <param name="obj"></param>
        void AddRange<T>(IEnumerable<T> list) where T : class;

        /// <summary>
        /// Возвращает запись по заданному идентификатору-
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns></returns>
        TEntity GetById(int id);

        /// <summary>
        /// Возвращает запись по выражению
        /// </summary>
        /// <param name="expression">Выражение</param>
        /// <returns></returns>
        T SingleOrDefault<T>(Expression<Func<T, bool>> expression) where T : class;

        /// <summary>
        /// Возвращает все записи
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Обновляет запись в БД
        /// </summary>
        /// <param name="obj"></param>
        void Update(TEntity obj);

        /// <summary>
        /// Удаляет запись по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        void Remove(int id);

        /// <summary>
        /// Удаляет запись в БД
        /// </summary>
        void Remove(TEntity obj);

        /// <summary>
        /// Сохраняет измененный контекст в базу данных
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
    }
}
