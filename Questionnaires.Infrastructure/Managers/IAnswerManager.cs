﻿using Questionnaires.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaires.Infrastructure.Managers
{
    public interface IAnswerManager
    {
        List<Answer> ListAnswers();
        string Save(List<Question> questions);
        //Answer GetProject(int id);
        //Answer CreateAnswer(Answer answer);
        //Answer EditAnswer(Answer answer);
        //void RemoveAnswer(Answer answer);
        void Dispose();
    }
}
