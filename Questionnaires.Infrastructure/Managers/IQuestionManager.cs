﻿using Questionnaires.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaires.Infrastructure.Managers
{
    public interface IQuestionManager
    {
        List<Question> ListQuestion();
        Question GetQuestion(int id);
        Question CreateQuestion(Question question);
        Question EditQuestion(Question question);
        //void RemoveQuestion(Question question);
        void Dispose();
        void EditQuestion(List<Question> questions);
    }
}
