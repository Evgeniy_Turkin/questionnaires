﻿
namespace Questionnaires.Entities
{
    public class Answer
    {
        /// <summary>
        /// Индификатов ответа
        /// </summary>
        public int AnswerId { get; set; }

        /// <summary>
        /// Ответ
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Индификационный номер вопроса
        /// </summary>
        public int QuestionId { get; set; }

        public virtual Question Question { get; set; }

    }
}
