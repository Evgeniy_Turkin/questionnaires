﻿using System.Collections.Generic;

namespace Questionnaires.Entities
{
    public class Question
    {
        /// <summary>
        /// Идентификатор вопроса
        /// </summary>
        public int QuestionId { get; set; }

        /// <summary>
        /// Вопрос
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Тип вопроса
        /// </summary>
        public string Type { get; set; }

        /////// <summary>
        /////// Ответ на вопрос
        /////// </summary>
        public string Answer { get; set; }
    }
}
