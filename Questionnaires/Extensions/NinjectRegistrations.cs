﻿using Ninject.Modules;
using Questionnaires.BusinessLayer;
using Questionnaires.DataLayer;
using Questionnaires.Infrastructure;
using Questionnaires.Infrastructure.Managers;

namespace Questionnaires.Extensions
{
    public class NinjectRegistrations :  NinjectModule
    {
        /// <summary>
        /// Регистрация зависимостей
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<IQuestionManager>().To<QuestionManager>();
            Bind<IAnswerManager>().To<AnswerManager>();
        }
    }
}