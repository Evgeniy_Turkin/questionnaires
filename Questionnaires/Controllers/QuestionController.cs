﻿using Ninject;
using Questionnaires.Core.Enum;
using Questionnaires.Entities;
using Questionnaires.Infrastructure.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Questionnaires.Controllers
{
    public class QuestionController : Controller
    {
        private readonly IKernel _container;
        private IQuestionManager _questionManager;

        public QuestionController(IKernel container)
        {
            _container = container;
            _questionManager = container.Get<IQuestionManager>();
        }

        // GET: Question
        public ActionResult Index()
        {
            var questions = _questionManager.ListQuestion();
            return View(questions);
        }

        public ActionResult Create(int id = 0)
        {
            ViewBag.Type = new SelectList(Enum.GetNames(typeof(QuestionDataType)).ToList());
            return View(new Question());
        }

        [HttpPost]
        public ActionResult Create(Question question)
        {
            _questionManager.CreateQuestion(question);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id = 0)
        {
            var question = _questionManager.GetQuestion(id);
            return View(question);
        }

        [HttpPost]
        public ActionResult Edit(List<Question> questions)
        {
            _questionManager.EditQuestion(questions);
            return RedirectToAction("Index");
        }


        public new void Disposed()
        {
            _questionManager.Dispose();
        }

    }
}