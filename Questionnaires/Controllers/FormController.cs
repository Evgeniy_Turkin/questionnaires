﻿using Ninject;
using Questionnaires.Entities;
using Questionnaires.Infrastructure.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Questionnaires.Controllers
{
    public class FormController : Controller
    {
        private readonly IKernel _container;
        private IQuestionManager _questionManager;
        private IAnswerManager _answerManager;

        public FormController(IKernel container)
        {
            _container = container;
            _questionManager = container.Get<IQuestionManager>();
            _answerManager = container.Get<IAnswerManager>();
        }

        // GET: Form
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TestView()
        {
            var questions = _questionManager.ListQuestion();
            return View(questions);
        }

        public JsonResult GetQuestions()
        {
            var questions = _questionManager.ListQuestion();
            return new JsonResult { Data = questions, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult SaveAnswers(List<Question> questions)
        {
            var answer= _answerManager.Save(questions);
            return new JsonResult { Data = answer, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}