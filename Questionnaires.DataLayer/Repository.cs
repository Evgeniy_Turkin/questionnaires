﻿using Questionnaires.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Questionnaires.DataLayer
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected ApplicationDbContext Db;
        protected DbSet<TEntity> DbSet;

        public Repository(ApplicationDbContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Add(TEntity obj)
        {
            DbSet.Add(obj);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }

        public T SingleOrDefault<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return Db.Set<T>().SingleOrDefault(expression);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Update(TEntity obj)
        {
            Db.Set<TEntity>().Attach(obj);
            Db.Entry(obj).State = EntityState.Modified;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Remove(int id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public virtual void Remove(TEntity obj)
        {
            DbSet.Remove(obj);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public void AddRange<T>(IEnumerable<T> list) where T : class
        {
            Db.Set<T>().AddRange(list);
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public int SaveChanges()
        {
            return Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
