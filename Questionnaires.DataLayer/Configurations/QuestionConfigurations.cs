﻿using Questionnaires.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Questionnaires.DataLayer.Configurations
{
    public class QuestionConfigurations : EntityTypeConfiguration<Question>
    {
        public QuestionConfigurations()
        {
            ToTable("Questions");

            HasKey(c => c.QuestionId);

            Property(c => c.Text)
            .HasMaxLength(255)
            .IsRequired();

            Property(c => c.Type)
            .HasMaxLength(255)
            .IsRequired();

            Property(c => c.Answer)
            .HasMaxLength(255)
            .IsRequired();
        }
    }
}
