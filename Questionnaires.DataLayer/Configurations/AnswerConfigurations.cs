﻿using Questionnaires.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Questionnaires.DataLayer.Configurations
{
    public class AnswerConfigurations : EntityTypeConfiguration<Answer>
    {
        public AnswerConfigurations()
        {
            ToTable("Answers");

            HasKey(c => c.AnswerId);

            Property(c => c.Text)
                .HasMaxLength(255)
                .IsRequired();

            HasRequired(t => t.Question)
                .WithMany()
                .HasForeignKey(d => d.QuestionId);
        }
    }
}
