﻿using Questionnaires.DataLayer.Configurations;
using Questionnaires.Entities;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Questionnaires.DataLayer
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    public class ApplicationDbContext : DbContext
    {

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Question { get; set; }

        public ApplicationDbContext() : base(ConfigurationManager.AppSettings["EntitiesDataBaseCatalog"])
        {
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Configurations.Add(new AnswerConfigurations());
            builder.Configurations.Add(new QuestionConfigurations());

            DisableOneToManyCascadeDelete(builder);
        }

        /// <summary>
        /// Отключает каскадное удаление
        /// </summary>
        /// <param name="builder"></param>
        private static void DisableOneToManyCascadeDelete(DbModelBuilder builder)
        {
            builder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            builder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
